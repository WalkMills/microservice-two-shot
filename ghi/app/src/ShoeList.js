import { NavLink } from "react-router-dom";
import { useState, useEffect } from "react";

function ShoeList () {

    const [shoes, setShoes] = useState([])

    const loadShoes = async () => {
        const response = await fetch('http://localhost:8080/api/shoes/')

        if (response.ok){
          const data = await response.json()
          setShoes(data.shoes)
        } else {
          console.error(response)
        }
      }



    const handleClick = async (event) => {

        const shoeID = event.target.value


        const url = `http://localhost:8080/api/shoes/${shoeID}/`

        const fetchConfig = {
            method: 'delete'
        }

        const response = await fetch(url, fetchConfig)

        if (response.ok) {
            setShoes(shoes.filter(item => item.id !== Number(shoeID)))

        }
    }

    useEffect(() => {
        loadShoes();
    }, []);



    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Shoe Name</th>
                        <th>Color</th>
                        <th>Closet Name</th>
                    </tr>
                </thead>
                <tbody>
                    { shoes.map(shoe => {
                        return (
                            <tr id="shoeList" key={shoe.id} >
                                <td>{ shoe.manufacturer }</td>
                                <td>{ shoe.shoe_name }</td>
                                <td>{ shoe.color }</td>
                                <td>{ shoe.closet_name }</td>
                                <td><button value={shoe.id} onClick={handleClick} className="btn btn-outline-success" type="button">Delete</button></td>
                            </tr>
                        );
                    }) }
                </tbody>
            </table>
            <NavLink to="/shoes/new"><button className="btn btn-success">New Shoe</button></NavLink>
        </div>
    );
}

export default ShoeList;
