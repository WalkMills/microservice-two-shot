import { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";


function ShoeForm () {

    const [bins, setBins] = useState([])
    const [manufacturer, setManu] = useState('')
    const [shoeName, setShoeName] = useState('')
    const [color, setColor] = useState('')
    const [shoePic, setShoePic] = useState('')
    const [bin, setBin] = useState('')

    const fetchData = async () => {
        const url = "http://localhost:8100/api/bins/"
        const response = await fetch(url)
        if (response.ok){
            const data = await response.json()
            setBins(data.bins)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleChange = (setFunction) => {
        return (
            function (event) {
                setFunction(event.target.value)
            }
        )
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}

        data.manufacturer = manufacturer
        data.shoe_name = shoeName
        data.color = color
        data.picture_url = shoePic
        data.bin = bin

        const shoeUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json()

            setManu('')
            setShoeName('')
            setColor('')
            setShoePic('')
            setBin('')

            document.getElementById('create-shoe-form').reset()
        }
    }

    return (
        <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <NavLink to="/shoes"><button className="btn btn-success">Back</button></NavLink>
                    <h1>Create a new Shoe</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleChange(setManu)} required={true} placeholder="Manufacturer" tpye="text" name="manufacturer" id="manufacturer" className="form-control"/>
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChange(setShoeName)} required={true}  placeholder="Shoe Name" tpye="text" name="shoeName" id="shoeName" className="form-control"/>
                            <label htmlFor="shoeName">Shoe Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChange(setColor)} required={true}  placeholder="Color" tpye="text" name="color" id="color" className="form-control"/>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChange(setShoePic)} placeholder="Shoe Picture" tpye="url" name="shoePic" id="shoePic" className="form-control"/>
                            <label htmlFor="shoePic">Shoe Picture</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleChange(setBin)} required={true} id="bin" name="bin" className="form-select">
                                <option value="">Which Bin?</option>
                                {bins.map(bin =>{
                                    return (
                                        <option key={bin.id} value={bin.href}>
                                            {bin.closet_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-success">Create</button>
                    </form>
                </div>
            </div>
        </div>
        </>
    );
}

export default ShoeForm;
