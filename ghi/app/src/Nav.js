import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark" style={{backgroundColor: "#198754"}}>
      <div className="container-fluid">
        <NavLink className="navbar-brand" style={{color: "white", opacity: 1}} to="/">Wardrobify</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" style={{color: "white", opacity: 1}} aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" style={{color: "white", opacity: 1}} to="/shoes">Shoes</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" style={{color: "white", opacity: 1}} to="/hats">Hats</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
