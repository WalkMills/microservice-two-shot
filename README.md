# Wardrobify

Team:

* Walker - Shoe
* Antonio - hat

## Design

## Shoes microservice

Created my Shoe model based on the learn requirements and my BinVO based on the Bin model inside of wardrobe_api

Updated the poller file to send a GET request to the wardrobe microservice at http://wardrobe-api:8100/api/bins and return with an up to date list of all bins

Then used the bin href to create view functions handle all of my HTTP requests through encoders and filtering by HTTP verbs to to allowing me to create, delete, or view a list of shoes

Finally using react, created a frontend experience that allowed users to create a shoe through a 'new shoe form' and to view a list of all shoes as well as delete a shoe if desired


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
