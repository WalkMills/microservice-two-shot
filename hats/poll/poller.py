import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

# Import models from hats_rest, here.
# from hats_rest.models import Something

def poll():
    while True:
        try:
            url = "http://wardrobe-api:8000/api/locations/"
            response = requests.get(url)
            content = json.loads(response.content)
            print(content)
            for location in content["locations"]:
                print(location)
                print(content[0])
                LocationVO.objects.update_or_create(
                    import_href = location['href'],
                    defaults={
                        "closet_name": location["closet_name"],
                        "section_number": location["section_number"],
                        "shelf_number": location["shelf_number"]},
                    )


        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(10)


if __name__ == "__main__":
    poll()

# GET http://wardrobe-api:8000/api/locations/
# GET http://wardrobe-api:8000/api/locations/<int:pk>/
# POST http://wardrobe-api:8000/api/locations/
# PUT http://wardrobe-api:8000/api/locations/<int:pk>/
# DELETE http://wardrobe-api:8000/api/locations/<int:pk>/
