from django.db import models


class BinVO (models.Model):
    import_href = models.CharField(max_length=200, unique=True, default="")
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()


class Shoe (models.Model):

    manufacturer = models.CharField(max_length=150)
    shoe_name = models.CharField(max_length=50)
    color = models.CharField(max_length=30)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="bin",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name
