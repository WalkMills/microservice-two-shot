from .keys import PEXELS_API_KEY
import json
import requests

def get_photo(shoe):

    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": shoe}
    url = "https://api.pexels.com/v1/search"

    response = requests.get(url, params=params, headers=headers)
    content = response.content

    parsed_json = json.loads(content)
    picture = parsed_json["photos"][0]["src"]["original"]

    return {"picture_url": picture}
